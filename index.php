<?php
    
    $connection = mysqli_connect('localhost','root','','to-let');
    session_start();
    require_once('config.php');
    require_once('functions.php');

?>
<?php require_once('header.php'); ?>
<!DOCTYPE html>

<body>   
	<?php require_once('body.php');?>	
			 
   

	      <h3 class="design"> Rental Category </h3>         
				<div class="main">
				
					<ul class="to-let-menu-grid wow fadeInDown"> 
						<li>
							<a href="flat1.php">
								<span class="to-let-menu-icon to-let-menu-icon-flat"></span>
								<h3 class="to-let-menu-title">Flat</h3>
							</a>
						</li>
						<li>
							<a href="mess1.php">
								<span class="to-let-menu-icon to-let-menu-icon-mess"></span>
								<h3 class="to-let-menu-title">Mess</h3>
                            </a>
						</li>
						<li>
							<a href="hostel1.php">
								<span class="to-let-menu-icon to-let-menu-icon-hostel"></span>
								<h3 class="to-let-menu-title">Hostel</h3>
							</a>
						</li>
						<li>
							<a href="sub_let1.php">
								<span class="to-let-menu-icon to-let-menu-icon-sublet"></span>
								<h3 class="to-let-menu-title">Sub-Let</h3>
							</a>
						</li>
						<li>
							<a href="office1.php">
								<span class="to-let-menu-icon to-let-menu-icon-office"></span>
								<h3 class="to-let-menu-title">Office</h3>
							</a>
						</li>
						<li>
							<a href="comercial1.php">
								<span class="to-let-menu-icon to-let-menu-icon-commercial"></span>
								<h3 class="to-let-menu-title">Commercial<br>Space</h3>
							</a>
						</li>
						<li>
							<a href="garage1.php">
								<span class="to-let-menu-icon to-let-menu-icon-garage"></span>
								<h3 class="to-let-menu-title">Garage</h3>
							</a>
						</li>
						<li>
							<a href="tuition1.php">
								<span class="to-let-menu-icon to-let-menu-icon-tuition"></span>
								<h3 class="to-let-menu-title">Tuition</h3>
							</a>
						</li>
						<li>
							<a href="others1.php">
								<span class="to-let-menu-icon to-let-menu-icon-house_others"></span>
								<h3 class="to-let-menu-title">Others</h3>
							</a>
						</li>
						
					</ul>
					
				
				</div>
      </section>
		
			
			<section class="col-lg-4 col-md-4 col-sm-12">
				<div >
            <form id="advanced_search" action="post.php" method="POST">                        
                <br/><h4 style="text-align:center;color:#0197fd;">Find Your Service </h4>
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                <label for="">Type </label>
                                <select name="type" id="styleSelect5" class="form-control" required>
                                    <option value="">Select </option>
                                    <option value="1">Flat</option>
                  									<option value="2">Mess</option>
                  									<option value="3">Hostel</option>
                  									<option value="4">Sub-Let</option>
                  									<option value="5">Office</option>
                  									<option value="6">Commercial Space</option>
                  									<option value="7">Garage</option>
                                    <option value="8">tuition</option>
                                    <option value="9">others</option>  
								                </select>
               </div>
							<div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label>Division</label>
									<select id="division" name="city" class="form-control"  
                                    onchange="subdivision(this.value);"  >			
										<option value="">Select </option>
                                        <?php $sql="SELECT * FROM division";
                                              $query=mysqli_query($connection,$sql);
                                              while($row=mysqli_fetch_assoc($query)){                                     
                                               ?>
                                               <option value="<?php echo $row['id']; ?>"><?php echo $row['div_name']; ?> </option>
                                        <?php } ?>
                                        
								 </select>
							</div>

						   <div  id="sub_div1" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    
               </div>
               <div id="loadRegion1" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
               </div>     
			
                         
                 &nbsp;          
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
							
                      <input id="btnsubmit"  class="form-control btn btn-primary" type="submit" href="post.php" name="submit"  value="Search "> 
             </div>
						</form>
                        
				</div>
    
     </section>
			
		</aside>
      </div>
     </div> 
<br><br>

<script src="js/jquery.min.js"></script>
    <script src="js/tlstrap.min.js"></script>
    <script src="js/modernizr.custom.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/navmenu.js"></script>
    <script src="js/tlheader.min.js"></script>
    <script src="js/jquery.easing.js"></script>
    <script src="js/jquery.timeago.js"></script>
    <script src="js/scrolling.js"></script>
    <script src="js/tolet-ads-view.js"></script>
    <script src="js/tlselect.min.js"></script>
    <script type="text/javascript" src="js/tlshare.min.js"></script>
    <script type="text/javascript" src="js/tl.slider.mini.js"></script>
    <script type="text/javascript" src="js/imgslide.js"></script>
    <script type="text/javascript">
          $(document).ready(function() {
            $('img').on('click', function() {
              $("#showImg").empty();
              var image = $(this).attr("src");
              $("#showImg").append("<img class='img-responsive img-thumbnail' src='" + image + "' />")
            })
          });
    </script>
    <script>
        new gnMenu( document.getElementById( 'gn-menu' ) );
    </script>
   


    <script>
    jQuery(document).ready(function() {
    jQuery("time.timeago").timeago();
        });
    </script>
    <script>
        (function() {
            if (!String.prototype.trim) {
                (function() {
                    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                    String.prototype.trim = function() {
                        return this.replace(rtrim, '');
                    };
                })();
            }

            [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
                if( inputEl.value.trim() !== '' ) {
                    classie.add( inputEl.parentNode, 'input--filled' );
                }

                inputEl.addEventListener( 'focus', onInputFocus );
                inputEl.addEventListener( 'blur', onInputBlur );
            } );

            function onInputFocus( ev ) {
                classie.add( ev.target.parentNode, 'input--filled' );
            }

            function onInputBlur( ev ) {
                if( ev.target.value.trim() === '' ) {
                    classie.remove( ev.target.parentNode, 'input--filled' );
                }
            }
        })();
    </script>
    
    <script src="../ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> <script src="js/shortcut-fab.min.js"></script>
    <script>
    $(document).ready(function(){
        var links = [
            {
                "bgcolor":"rgba(41, 128, 185, 0.56)",
                "icon":"<i class='fa fa-plus'></i>"
            },
            {
                "url":"employment.php",
                "bgcolor":"",
                "color":"#fffff",
                "icon":"<i class='fa fa-briefcase'></i>",
                "target":"_blank"
            },
            {
                "url":"rent-a-car.php",
                "bgcolor":"",
                "color":"#fffff",
                "icon":"<i class='fa fa-car'></i>",
                "target":"_blank"
            },
            {
                "url":"houses.php",
                "bgcolor":"",
                "color":"#fffff",
                "icon":"<i class='fa fa-bed'></i>",
                "target":"_blank"
            },
            {
                "url":"post_free_ads.php",
                "bgcolor":"",
                "color":"fffff",
                "icon":"<i class='fa fa-pencil-square-o'></i>",
                "target":"_blank"
            },
            {
                "url":"/",
                "bgcolor":"",
                "color":"#fffff",
                "icon":"<i class='fa fa-home'></i>"
            }
        ];
        var options = {
            rotate: false
        };
        $('#shortcut').jqueryFab(links, options);
    })
    </script>
    <script type="text/javascript">
        
         function subdivision(id){
      
            $("#sub_div1").load('sub_division.php?id='+id);
        }
        $(document).on('change','#dept_wise_subject', function() {
        $("#btn").show();
       });
       function region(id){
        var division=$("#division option:selected").val();
       
        $("#loadRegion1").load('region.php?div_id='+division+'&sub_div_id='+id);
       }
    </script>




                <h2>Recent post :</h2>

              <?php $sql="SELECT * FROM service ORDER BY service_id desc limit 10 ";
                  $query=mysqli_query($connection,$sql);
                  while($row=mysqli_fetch_assoc($query)){  ?>
                   <div class="col-sm-3">
                      <div class="subProduct">
                         <span><?php 
                          if($row['type']=='1'){
                            echo "<h3 style='color:black; text-align:center;'>Flat</h3>";}?></span> 
                        <span><?php 
                          if($row['type']=='3'){
                            echo "<h3 style='color:black; text-align:center;'>Hostel</h3>";}?></span> 
                        <span><?php 
                          if($row['type']=='6'){
                            echo "<h3 style='color:black; text-align:center;'>Commercial space</h3>";}?></span> 
                        <span><?php 
                          if($row['type']=='5'){
                            echo "<h3 style='color:black; text-align:center;'>Office</h3>";}?></span> 
                        <span><?php 
                          if($row['type']=='4'){
                            echo "<h3 style='color:black; text-align:center;'>Sub-let</h3>";}?></span> 
                        <span><?php 
                          if($row['type']=='2'){
                            echo "<h3 style='color:black; text-align:center;'>Mess</h3>";}?></span> 

                            <span><?php 
                          if($row['type']=='8'){
                            echo "<h3 style='color:black; text-align:center;'>Tuition</h3>";}?></span>
                         <span><?php 
                          if($row['type']=='9'){
                            echo "<h3 style='color:black; text-align:center;'>Others</h3>";}?></span>
                            <span><?php 
                          if($row['type']=='7'){
                            echo "<h3 style='color:black; text-align:center;'>Garage</h3>";}?></span>
                         

                      <span> <a href="single_post.php"> <img src="<?php echo $row['photos']; ?>" height="200px"  width="260px"></a> <br></span>
                      <span><?php 
                          if($row['com_space_for']!=null){
                            echo "Commercial Space For: ".$row['com_space_for']; echo "<br />";  }?></span>
                      <span><?php 
                          if($row['car_capacity']!=null){
                            echo "Car Capacity: ".$row['car_capacity']; echo "<br />";  }?></span>
                       <span><?php 
                          if($row['tuition']!=null){

                            echo "Tuition Type: ".$row['tuition'];echo "<br />";  }?></span>
                        <span><?php 
                          if($row['cost']!=null){
                            echo "Expected Cost: ".$row['cost'];echo "<br />";   }?></span>
                      <span><?php 
                          if($row['bedroom']!=null){
                            echo "Bedroom: ".$row['bedroom']; echo "<br />";  }?></span>
                      <span><?php 
                          if($row['bathroom']!=null){
                            echo "Bathroom: ".$row['bathroom']; echo "<br />";  }?></span>
                      <span><?php 
                          if($row['room']!=null){
                            echo "Room: ".$row['room']; echo "<br />";  }?></span>
                     <span><?php 
                          if($row['size']!=null){
                            echo "Size: ".$row['size']; echo "<br />";  }?></span>
                     <span><?php 
                          if($row['rent']!=null){
                            echo "Rent: ".$row['rent']; echo "<br />";  }?></span>  
                     <span><?php 
                          if($row['address']!=null){
                            echo "Address: ".$row['address']; echo "<br />";  }?></span> 
                     <span><?php 
                          if($row['details']!=null){
                            echo "Details: ".$row['details']; echo "<br />";  }?></span> <br /> 
                     
                     <span><?php echo "<p style='color:black;font-weight: bold;'>Contact Info :</p>" ?></span>
                      <span><?php 
                          if($row['name']!=null){
                            echo "Name: ".$row['name'];   }?></span><br>
                      <span><?php 
                          if($row['email']!=null){
                            echo "Email: ".$row['email'];   }?></span><br>
                    
                      <span><?php echo "Phone: ".$row['phone']; ?></span>
                      <br><br><br>
                      </div>   
                   </div>      
                    <?php } ?>


   <div class="pagi">
         <nav aria-label="Page navigation">
            <ul class="pagination pagination-lg">
                <li>
                  <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                </li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="recent-post.php">2</a></li>
                <li><a href="recent-post.php">3</a></li>
                <li><a href="recent-post.php">4</a></li>
                <li><a href="recent-post.php">5</a></li>
                <li>
                  <a href="recent-post.php" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
              </a>
            </li>
          </ul>
        </nav>
   </div>
  
  
   
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript"></script>
</body>

<?php require_once('footer.php'); ?>


</html>


