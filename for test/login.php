<?php
  
  session_start();

  require_once('config.php');
  require_once('functions.php');

  if(user_logged_in()){
    header("location: admin/");
  }

    if(isset($_POST['register_submit'])){
      $firstname = $_POST['first_name'];
      $lastname = $_POST['last_name'];
      $email = $_POST['email_address'];
      $password = $_POST['password'];

      $encrypted_password = md5($password);

     /* */

        $rows = mysqli_query($connection, "SELECT userid FROM users WHERE email_address = '$email'");
          $totalrows = mysqli_num_rows($rows);
          if($totalrows >=1){
            $existerror = "Email address already taken";
          }
          else{
              $query = mysqli_query($connection, "INSERT INTO users (first_name,last_name,email_address,password) VALUES
             ('$firstname','$lastname','$email','$encrypted_password')");
          }
        }



        if(isset($_POST['loginsubmit'])){
            $emailaddress = $_POST['loginemail'];
            $loginpassword = $_POST['loginpassword'];

            $loginquery = mysqli_query($connection, "SELECT * FROM users WHERE email_address=
              '$emailaddress'");

            $output = mysqli_fetch_assoc($loginquery);

            $emailfromdb = $output['email_address'];
            $passwordfromdb = $output['password'];

            if($emailfromdb == $emailaddress && $passwordfromdb == md5($loginpassword)){
               
               $_SESSION['email']= $emailaddress;
               $_SESSION['email']= $output['password'];

                header('location: admin/index.php');
            }else{
                $loginerror = "Login credentials are invalid";
                header("location: #login");

            }
        }
    


?>






<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <link rel="stylesheet" href="style.css">
  <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="js/custom.js"></script>
</head>
<body>
 <?php
      if(isset($query)){
          echo "You have been registered";
        }
      if(isset($existerror)){
        echo $existerror;
      }
      if(isset($loginerror)){
        echo $loginerror;
      }
        ?> 
      

    <div class="form"> 
      
      <ul class="tab-group">
        <li class="tab active"><a href="#signup">Sign Up</a></li>
        <li class="tab"><a href="#login">Log In</a></li>
      </ul>
      
      
        <div id="signup">   
          <h1>Sign Up for Free</h1>
          
          <form action="" method="POST">
          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                First Name<span class="req">*</span>
              </label>
              <input type="text" required autocomplete="off" name="first_name" />
            </div>
        
            <div class="field-wrap">
              <label>
                Last Name<span class="req">*</span>
              </label>
              <input type="text"required autocomplete="off" name="last_name"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" name="email_address"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Set A Password<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" name="password"/>
          </div>
          
          <button type="submit" class="button button-block" name="register_submit">Get Started</button>
          
          </form>

        </div>
        
        <div id="login">   
          <h1>Welcome Back!</h1>
          
          <form action="" method="POST">
          
            <div class="field-wrap">
              <label>
                Email Address<span class="req">*</span>
              </label>
              <input type="email"required autocomplete="off" name="loginemail"/>
            </div>
          
            <div class="field-wrap">
              <label>
                Password<span class="req">*</span>
              </label>
              <input type="password"required autocomplete="off" name="loginpassword"/>
            </div>
          
          <p class="forgot"><a href="#">Forgot Password?</a></p>
          
          <button class="button button-block" name="loginsubmit"/>Log In</button>
          
          </form>

        </div>
        
      
      
    </div> <!-- /form -->
</body>
</html>