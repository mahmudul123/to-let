<?php
  session_start();
  require_once('config.php');
  if(isset($_POST['register_submit'])){
      $firstname = $_POST['first_name'];
      $lastname = $_POST['last_name'];
      $email = $_POST['email_address'];
      $password = $_POST['password'];

       $encrypted_password = md5($password);

        $rows = mysqli_query($connection,"SELECT id FROM users WHERE email_address = '$email'");
          $totalrows = mysqli_num_rows($rows);
          if($totalrows >=1){
            $existerror = "Email address already taken";
          }else{
              $query = mysqli_query($connection, "INSERT INTO users (first_name,last_name,email_address,password) VALUES
             ('$firstname','$lastname','$email','$encrypted_password')");

              header('location: welcome.php');
          }

  }

        if(isset($_POST['loginsubmit'])){
            $emailaddress = $_POST['loginemail'];
            $loginpassword = $_POST['loginpassword'];

            $loginquery = mysqli_query($connection, "SELECT * FROM users WHERE email_address=
              '$emailaddress'");

            $output = mysqli_fetch_assoc($loginquery);

            $emailfromdb = $output['email_address'];
            $passwordfromdb = $output['password'];

            if($emailfromdb == $emailaddress && $passwordfromdb == md5($loginpassword)){
               
               $_SESSION['email']= $emailaddress;
               $_SESSION['first_name']= $output['first_name'];
               $_SESSION['last_name']= $output['last_name'];

                header('location: index.php');
            }else{
                $loginerror = "Login credentials are invalid";
                header("location: register.php");

            }
        }




 ?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Registration</title>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" type="text/css" href="css/theme.css" />
  <script src="js/jquery.min.js"></script>
  <script src="js/custom.js"></script>
</head>
<body>
    
  <?php
      if(isset($query)){
          echo "you have been registered";
     }
     if(isset($existerror)){
      echo $existerror;
     }
     if(isset($loginerror)){
      echo $loginerror;
     }
  ?>

    <div class="form">
      
      <ul class="tab-group">

        <li class="tab "><a href="#login">Log In</a></li>
        <li class="tab active"><a href="#signup">Sign Up</a></li>
        
      </ul>
      
      <div class="tab-content">
        <div id="signup">   
          <h1>Sign Up for Free</h1>
          
          <form action="" method="POST">
          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                First Name<span class="req">*</span>
              </label>
              <input type="text" required autocomplete="off" name="first_name" />
            </div>
        
            <div class="field-wrap">
              <label>
                Last Name<span class="req">*</span>
              </label>
              <input type="text"required autocomplete="off" name="last_name"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Email Address
            </label>
            <input type="email"  name="email_address"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password
            </label>
            <input type="password"  name="password"/>
          </div>
          
          <button type="submit" class="button button-block" href="welcome.php" name="register_submit">Get Started</button>
          
          </form>

        </div>
        <div id="login">   
          <h1>Welcome Back</h1>
          
          <form action="" method="POST">
          
            <div class="field-wrap">
            <label>
              Email Address
            </label>
            <input type="email" name="loginemail"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password
            </label>
            <input type="password" name="loginpassword"/>
          </div>
          
          <p class="forgot"><a href="#">Forgot Password?</a></p>
          
          <button class="button button-block" name="loginsubmit"/>Log In</button>
          
          </form>

        </div>
        
        
        
        
      </div>
      
</div> 

</body>
</html>