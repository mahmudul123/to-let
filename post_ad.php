<?php
    
    require_once('config.php');
    require_once('functions.php');
    session_start();

    if(!user_logged_in()){
    header("location:register.php");
  }

?>
<?php require_once('header.php'); ?>
<body>   
		
			<div class="logo"> 
				<a href="index.php" class="imglogo"><img src="img/logo2.png" class="img-responsive logo" style="" alt="TO-LET.com.bd"/></a>
			</div>
		
<div class="header1"> 
			<div  class="btn">
                <?php if(isset($_SESSION['email'])){ ?>
            <li><a id="signin"  href="logout.php">&nbsp;&nbsp;Log Out</a><li>
                <?php }else{ ?>
            <li><a id="signup" href="register.php">Sign Up &nbsp;&nbsp;&nbsp;</a><li>
            <li><a id="signin"  href="login.php">&nbsp;&nbsp;Log In</a><li>
                <?php   } ?>        
             </div>
            <div class="user">
                 <i class="fa fa-user" aria-hidden="true"></i>
                 <span style="background-color:#C4DFF6;padding:4px;border-radius: 5px;"><?php if (isset($_SESSION['email'])){
                 echo $_SESSION['first_name'].' '.$_SESSION['last_name'];
                 } ?></span>
            </div>
			<div class="menu1"> 
				<ul>
					 <li><a href="index.php">Home</a></li>
                    <li><a href="http://localhost/to-let/recent-post.php">Recent Post</a></li>
                    <li><a href="about_us.php">About Us</a></li>
                    <li><a href="contact_us.php">Contact Us</a></li>
                    <li><a href="conditions.php">Terms & Condition</a></li>
				</ul>
			</div>
			
		
	</div>
	


    <div class="container-fluid">
    	<div class="row">
    		<div class="home-flash">
			</div>
        	<section class="col-sm-8 col-sm-push-2">
				<div class="row">
					
				</div>
			 
				<h2>Select To Post Ad:</h2><br />        
				<div class="main">
				
					<ul class="to-let-menu-grid wow fadeInDown"> 
						<li>
							<a href="flat.php">
								<span class="to-let-menu-icon to-let-menu-icon-flat"></span>
								<h3 class="to-let-menu-title">Flat</h3>
							</a>
						</li>
						<li>
							<a href="mess.php">
								<span class="to-let-menu-icon to-let-menu-icon-mess"></span>
								<h3 class="to-let-menu-title">Mess</h3>
							</a>
						</li>
						<li>
							<a href="hostel.php">
								<span class="to-let-menu-icon to-let-menu-icon-hostel"></span>
								<h3 class="to-let-menu-title">Hostel</h3>
							</a>
						</li>
						<li>
							<a href="sub-let.php">
								<span class="to-let-menu-icon to-let-menu-icon-sublet"></span>
								<h3 class="to-let-menu-title">Sub-Let</h3>
							</a>
						</li>
						<li>
							<a href="office.php">
								<span class="to-let-menu-icon to-let-menu-icon-office"></span>
								<h3 class="to-let-menu-title">Office</h3>
							</a>
						</li>
						<li>
							<a href="com_space.php">
								<span class="to-let-menu-icon to-let-menu-icon-commercial"></span>
								<h3 class="to-let-menu-title">Commercial<br>Space</h3>
							</a>
						</li>
						<li>
							<a href="garage.php">
								<span class="to-let-menu-icon to-let-menu-icon-garage"></span>
								<h3 class="to-let-menu-title">Garage</h3>
							</a>
						</li>
						<li>
							<a href="tuition.php">
								<span class="to-let-menu-icon to-let-menu-icon-tuition"></span>
								<h3 class="to-let-menu-title">Tuition</h3>
							</a>
						</li>
						<li>
							<a href="others.php">
								<span class="to-let-menu-icon to-let-menu-icon-house_others"></span>
								<h3 class="to-let-menu-title">Others</h3>
							</a>
						</li>
						
					</ul>
					
				
				</div>
            </section>
		
			
         </div>
     </div>    

   <?php require_once('footer.php'); ?>   

    <script src="js/jquery.min.js"></script>
    <script src="js/tlstrap.min.js"></script>
    <script src="js/modernizr.custom.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/navmenu.js"></script>
    <script src="js/tlheader.min.js"></script>
    <script src="js/jquery.easing.js"></script>
    <script src="js/jquery.timeago.js"></script>
    <script src="js/scrolling.js"></script>
    <script src="js/tolet-ads-view.js"></script>
    <script src="js/tlselect.min.js"></script>
    <script type="text/javascript" src="js/tlshare.min.js"></script>
    <script type="text/javascript" src="js/tl.slider.mini.js"></script>
    <script type="text/javascript" src="js/imgslide.js"></script>
    <script type="text/javascript">
          $(document).ready(function() {
            $('img').on('click', function() {
              $("#showImg").empty();
              var image = $(this).attr("src");
              $("#showImg").append("<img class='img-responsive img-thumbnail' src='" + image + "' />")
            })
          });
    </script>
    <script>
        new gnMenu( document.getElementById( 'gn-menu' ) );
    </script>
    <script>
    $("#e1").select2({
    placeholder: "Area/Place You Live",
    allowClear: true
});
    </script>
    <script>
    $("#e2").select2({
    placeholder: "All Location in Dhaka",
    allowClear: true
});
    </script>
    <script>
    jQuery(document).ready(function() {
    jQuery("time.timeago").timeago();
        });
    </script>
    <script>
        (function() {
            if (!String.prototype.trim) {
                (function() {
                    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                    String.prototype.trim = function() {
                        return this.replace(rtrim, '');
                    };
                })();
            }

            [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
                if( inputEl.value.trim() !== '' ) {
                    classie.add( inputEl.parentNode, 'input--filled' );
                }

                inputEl.addEventListener( 'focus', onInputFocus );
                inputEl.addEventListener( 'blur', onInputBlur );
            } );

            function onInputFocus( ev ) {
                classie.add( ev.target.parentNode, 'input--filled' );
            }

            function onInputBlur( ev ) {
                if( ev.target.value.trim() === '' ) {
                    classie.remove( ev.target.parentNode, 'input--filled' );
                }
            }
        })();
    </script>
    <script src="../ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> <script src="js/shortcut-fab.min.js"></script>
    <script>
    $(document).ready(function(){
        var links = [
            {
                "bgcolor":"rgba(41, 128, 185, 0.56)",
                "icon":"<i class='fa fa-plus'></i>"
            },
            {
                "url":"employment.php",
                "bgcolor":"",
                "color":"#fffff",
                "icon":"<i class='fa fa-briefcase'></i>",
                "target":"_blank"
            },
            {
                "url":"rent-a-car.php",
                "bgcolor":"",
                "color":"#fffff",
                "icon":"<i class='fa fa-car'></i>",
                "target":"_blank"
            },
            {
                "url":"houses.php",
                "bgcolor":"",
                "color":"#fffff",
                "icon":"<i class='fa fa-bed'></i>",
                "target":"_blank"
            },
            {
                "url":"post_free_ads.php",
                "bgcolor":"",
                "color":"fffff",
                "icon":"<i class='fa fa-pencil-square-o'></i>",
                "target":"_blank"
            },
            {
                "url":"/",
                "bgcolor":"",
                "color":"#fffff",
                "icon":"<i class='fa fa-home'></i>"
            }
        ];
        var options = {
            rotate: false
        };
        $('#shortcut').jqueryFab(links, options);
    })
    </script>

</body>

<!-- Mirrored from www.to-let.com.bd/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 11 Dec 2016 12:05:06 GMT -->
</html>
<div align="center" title="Copyright">TO-LET &copy; 2016</div>

