    <script type="text/javascript" src="js/jquery-1.11.3.min.js"> </script>
    <script type="text/javascript" src="js/jquery-ui.min.js"> </script>
     <script src="js/jquery.min.js"></script>
    <script src="js/tlstrap.min.js"></script>
    <script src="js/modernizr.custom.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/navmenu.js"></script>
    <script src="js/tlheader.min.js"></script>
    <script src="js/jquery.easing.js"></script>
    <script src="js/jquery.timeago.js"></script>
    <script src="js/scrolling.js"></script>
    <script src="js/tolet-ads-view.js"></script>
    <script src="js/tlselect.min.js"></script>
    <script type="text/javascript" src="js/tlshare.min.js"></script>
    <script type="text/javascript" src="js/tl.slider.mini.js"></script>
    <script type="text/javascript" src="js/imgslide.js"></script>
    <script type="text/javascript">

          $(document).ready(function() {
            $('img').on('click', function() {
              $("#showImg").empty();
              var image = $(this).attr("src");
              $("#showImg").append("<img class='img-responsive img-thumbnail' src='" + image + "' />")
            })
          });
    </script>
    <script>
        new gnMenu( document.getElementById( 'gn-menu' ) );
    </script>
    <script>
    $("#e1").select2({
    placeholder: "Area/Place You Live",
    allowClear: true
});
    </script>
    <script>
    $("#e2").select2({
    placeholder: "All Location in Dhaka",
    allowClear: true
});
    </script>
    <script>
    jQuery(document).ready(function() {
    jQuery("time.timeago").timeago();
        });
    </script>
    <script>
        (function() {
            if (!String.prototype.trim) {
                (function() {
                    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                    String.prototype.trim = function() {
                        return this.replace(rtrim, '');
                    };
                })();
            }

            [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
                if( inputEl.value.trim() !== '' ) {
                    classie.add( inputEl.parentNode, 'input--filled' );
                }

                inputEl.addEventListener( 'focus', onInputFocus );
                inputEl.addEventListener( 'blur', onInputBlur );
            } );

            function onInputFocus( ev ) {
                classie.add( ev.target.parentNode, 'input--filled' );
            }

            function onInputBlur( ev ) {
                if( ev.target.value.trim() === '' ) {
                    classie.remove( ev.target.parentNode, 'input--filled' );
                }
            }
        })();
    </script>
    <script src="../ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> <script src="js/shortcut-fab.min.js"></script>
    <script>
    $(document).ready(function(){
        var links = [
            {
                "bgcolor":"rgba(41, 128, 185, 0.56)",
                "icon":"<i class='fa fa-plus'></i>"
            },
            {
                "url":"employment.php",
                "bgcolor":"",
                "color":"#fffff",
                "icon":"<i class='fa fa-briefcase'></i>",
                "target":"_blank"
            },
            {
                "url":"rent-a-car.php",
                "bgcolor":"",
                "color":"#fffff",
                "icon":"<i class='fa fa-car'></i>",
                "target":"_blank"
            },
            {
                "url":"houses.php",
                "bgcolor":"",
                "color":"#fffff",
                "icon":"<i class='fa fa-bed'></i>",
                "target":"_blank"
            },
            {
                "url":"post_free_ads.php",
                "bgcolor":"",
                "color":"fffff",
                "icon":"<i class='fa fa-pencil-square-o'></i>",
                "target":"_blank"
            },
            {
                "url":"/",
                "bgcolor":"",
                "color":"#fffff",
                "icon":"<i class='fa fa-home'></i>"
            }
        ];
        var options = {
            rotate: false
        };
        $('#shortcut').jqueryFab(links, options);
    })
    </script>